@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details EchelleLineaire') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('echelle_lineaires.edit', $echelle_lineaire->id) }}">Edit</a>
                        <form action="{{ route('echelle_lineaires.destroy', $echelle_lineaire->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $echelle_lineaire->debut }} :</strong> 
                    </li>
                        <p>{{ $echelle_lineaire->fin }} </p>
                        <br> Added at {{  $echelle_lineaire->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection