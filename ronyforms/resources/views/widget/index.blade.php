@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Liste des Widgets') }}</div>
  
                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <ol>
                        @foreach($widgets as $widget)
                            <a href="{{ route('widgets.show', $widget->id) }}"><li>  <strong>{{ $widget->titre }} :</strong></a>
                           
                            <p>{{ $widget->description }}</p>
                            <br> Added at {{  $widget->created_at->diffForHumans() }}
                            <br>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection