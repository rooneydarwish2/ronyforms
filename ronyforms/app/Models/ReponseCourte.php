<?php
// php artisan make:model ReponseCourte -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReponseCourte extends Model
{
    use HasFactory;

    protected $fillable = ['contenu'];

    // ManyToOne
    public function widget(){
        return $this->belongsTo(Widget::class);
    }
}
