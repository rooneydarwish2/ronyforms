<?php
// commande :  php artisan make:controller ChoixMultipleController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ChoixMultiple;

class ChoixMultipleController extends Controller
{

    public function create_with_widget_id($widget_id)
    {
        return view('choix_multiple/create', 
                    ['widget_id' => $widget_id]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $choix_multiples = ChoixMultiple::all();
        
        return view('choix_multiple/index',compact('choix_multiples'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('choix_multiple/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_widget_id(Request $request, $widget_id)
    {
        $validatedData = $request->validate([
                    'contenu' => 'required|max:100',
                    'selectionne' => 'required'
                ]);
        $show = new ChoixMultiple($validatedData);
        $show->widget_id = $widget_id;
        $show->save();
        
        return redirect('/choix_multiples')->with('success', 'ChoixMultiple is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
            'contenu' => 'required|max:100',
            'selectionne' => 'required'
        ]);
        $show = new ChoixMultiple($validatedData);
        $show->user_id = $user->id;
        $show->section_id = 1;
        $show->save();
        
        return redirect('/choix_multiples')->with('success', 'ChoixMultiple is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $choix_multiple = ChoixMultiple::findOrFail($id);
        return view('choix_multiple/show',compact('choix_multiple'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $choix_multiple = ChoixMultiple::findOrFail($id);

        return view('choix_multiple/edit', compact('choix_multiple'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'contenu' => 'required|max:100',
            'selectionne' => 'required'
        ]);
        ChoixMultiple::whereId($id)->update($validatedData);

        return redirect('/choix_multiples')->with('success', 'ChoixMultiple Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $choix_multiple = ChoixMultiple::findOrFail($id);
        $choix_multiple->delete();

        return redirect('/choix_multiples')->with('success', 'ChoixMultiple Data is successfully deleted');
    }
}
