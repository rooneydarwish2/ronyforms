@extends('layout')

@section('title', 'Home')

@section('content')
<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Veuillez renseigner les informations du echelle_lineaire</div>
                  <div class="card-body">
  
                      <form action="{{ route('echelle_lineaires.update', $echelle_lineaire->id) }}" method="POST">
                          @csrf
                          @method('PUT')
                          
                          <div class="form-group row">
                              <label for="debut" class="col-md-4 col-form-label text-md-right">debut</label>
                              <div class="col-md-6">
                                  <input type="text" id="debut" class="form-control" value="{{ $echelle_lineaire->debut }}" name="debut" required autofocus>
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="fin" class="col-md-4 col-form-label text-md-right">fin</label>
                              <div class="col-md-6">
                                  <input type="text" id="fin" class="form-control" value="{{ $echelle_lineaire->fin }}" name="fin" required>
                              </div>
                          </div>
  
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Submitter
                              </button>
                          </div>
                      </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
@endsection