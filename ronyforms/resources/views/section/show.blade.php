@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details Section') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('sections.edit', $section->id) }}">Edit</a>
                        <form action="{{ route('sections.destroy', $section->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $section->titre }} :</strong> 
                    </li>
                        <p>{{ $section->description }} </p>
                        <br> Added at {{  $section->created_at->diffForHumans() }}
                        @foreach($section->sous_sections as $section)
                            <li>{{ $section->titre }}</li>
                        @endforeach
                        
                        <a href="{{ route('sous_sections.create_with_section_id', $section->id) }}">+ SousSection</a>

                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection