<?php
// php artisan make:model ChoixMultiple -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChoixMultiple extends Model
{
    use HasFactory;

    protected $fillable = ['contenu', 'selectionne'];

    // ManyToOne
    public function widget(){
        return $this->belongsTo(Widget::class);
    }
}
