@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <h1>Rony Rony</h1> 
                    Hi and welcome to my testing page!
                    <a href="{{ route('formulaires.create') }}">Creer un formulaire</a><br>
                    <a href="{{ route('formulaires.index') }}">liste forms</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection