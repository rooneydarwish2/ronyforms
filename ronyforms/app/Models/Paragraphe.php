<?php
// php artisan make:model Paragraphe -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paragraphe extends Model
{
    use HasFactory;

    protected $fillable = ['contenu'];

    // ManyToOne
    public function widget(){
        return $this->belongsTo(Widget::class);
    }
}
