<?php
// commande : php artisan make:controller ReponseCourteController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReponseCourte;

class ReponseCourteController extends Controller
{
    public function create_with_widget_id($widget_id)
    {
        return view('reponse_courte/create', 
                    ['widget_id' => $widget_id]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reponse_courtes = ReponseCourte::all();
        
        return view('reponse_courte/index',compact('reponse_courtes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reponse_courte/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_widget_id(Request $request, $widget_id)
    {
        $validatedData = $request->validate([
                    'contenu' => 'required|max:100'
                ]);
        $show = new ReponseCourte($validatedData);
        $show->widget_id = $widget_id;
        $show->save();
        
        return redirect('/reponse_courtes')->with('success', 'ReponseCourte is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
            'contenu' => 'required|max:100'
        ]);
        $show = new ReponseCourte($validatedData);
        $show->user_id = $user->id;
        $show->section_id = 1;
        $show->save();
        
        return redirect('/reponse_courtes')->with('success', 'ReponseCourte is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reponse_courte = ReponseCourte::findOrFail($id);
        return view('reponse_courte/show',compact('reponse_courte'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reponse_courte = ReponseCourte::findOrFail($id);

        return view('reponse_courte/edit', compact('reponse_courte'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'contenu' => 'required|max:100'
        ]);
        ReponseCourte::whereId($id)->update($validatedData);

        return redirect('/reponse_courtes')->with('success', 'ReponseCourte Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reponse_courte = ReponseCourte::findOrFail($id);
        $reponse_courte->delete();

        return redirect('/reponse_courtes')->with('success', 'ReponseCourte Data is successfully deleted');
    }
}
