@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details DateHeure') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('date_heures.edit', $date_heure->id) }}">Edit</a>
                        <form action="{{ route('date_heures.destroy', $date_heure->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $date_heure->valeur }} :</strong> 
                    </li>
                        <p>{{ $date_heure->selectionne }} </p>
                        <br> Added at {{  $date_heure->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection