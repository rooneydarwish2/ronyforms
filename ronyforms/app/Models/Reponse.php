<?php
// php artisan make:model Reponse -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{
    use HasFactory;

    // OneToMany
    public function reponses(){
        return $this->hasMany(Repondre::class);
    }

    // ManyToOne
    public function widget(){
        return $this->belongsTo(Widget::class);
    }
}
