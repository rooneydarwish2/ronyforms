@extends('layout')

@section('title', 'Home')

@section('content')
<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Veuillez renseigner les informations du cases_acocher</div>
                  <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif

                    <form action="{{ route('cases_acochers.store_with_widget_id', $widget_id) }}" method="POST">
                          @csrf
                          <div class="form-group row">
                              <label for="contenu" class="col-md-4 col-form-label text-md-right">Contenu</label>
                              <div class="col-md-6">
                                  <input type="text" id="contenu" class="form-control" name="contenu" required autofocus>
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="coche" class="col-md-4 col-form-label text-md-right">coche</label>
                              <div class="col-md-6">
                                  <input type="text" id="coche" class="form-control" name="coche">
                              </div>
                          </div>
  
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Ajouter
                              </button>
                          </div>
                    </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
@endsection