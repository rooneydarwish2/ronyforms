<?php
// php artisan make:controller SousSectionController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SousSection;
use Illuminate\Support\Facades\Auth;

class SousSectionController extends Controller
{
    public function create_with_section_id($section_id)
    {
        return view('sous_section/create', ['section_id' => $section_id]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sous_sections = SousSection::all();
        
        return view('sous_section/index',compact('sous_sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sous_section/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_section_id(Request $request, $section_id)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
                    'titre' => 'required|max:255',
                    'description' => 'required'
                ]);
        $show = new SousSection($validatedData);
        $show->user_id = $user->id;
        $show->section_id = $section_id;
        $show->save();
        
        return redirect('/sous_sections')->with('success', 'SousSection is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
                    'titre' => 'required|max:255',
                    'description' => 'required'
                ]);
        $show = new SousSection($validatedData);
        $show->user_id = $user->id;
        $show->section_id = 1;
        $show->save();
        
        return redirect('/sous_sections')->with('success', 'SousSection is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sous_section = SousSection::findOrFail($id);
        return view('sous_section/show',compact('sous_section'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sous_section = SousSection::findOrFail($id);

        return view('sous_section/edit', compact('sous_section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'titre' => 'required|max:255',
            'description' => 'required'
        ]);
        SousSection::whereId($id)->update($validatedData);

        return redirect('/sous_sections')->with('success', 'SousSection Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sous_section = SousSection::findOrFail($id);
        $sous_section->delete();

        return redirect('/sous_sections')->with('success', 'SousSection Data is successfully deleted');
    }
}
