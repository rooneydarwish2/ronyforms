<?php
// commande : php artisan make:controller ParagrapheController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paragraphe;

class ParagrapheController extends Controller
{
    public function create_with_widget_id($widget_id)
    {
        return view('paragraphe/create', 
                    ['widget_id' => $widget_id]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paragraphes = Paragraphe::all();
        
        return view('paragraphe/index',compact('paragraphes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paragraphe/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_widget_id(Request $request, $widget_id)
    {
        $validatedData = $request->validate([
                    'contenu' => 'required|max:100'
                ]);
        $show = new Paragraphe($validatedData);
        $show->widget_id = $widget_id;
        $show->save();
        
        return redirect('/paragraphes')->with('success', 'Paragraphe is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
            'contenu' => 'required|max:100'
        ]);
        $show = new Paragraphe($validatedData);
        $show->user_id = $user->id;
        $show->section_id = 1;
        $show->save();
        
        return redirect('/paragraphes')->with('success', 'Paragraphe is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paragraphe = Paragraphe::findOrFail($id);
        return view('paragraphe/show',compact('paragraphe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paragraphe = Paragraphe::findOrFail($id);

        return view('paragraphe/edit', compact('paragraphe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'contenu' => 'required|max:100'
        ]);
        Paragraphe::whereId($id)->update($validatedData);

        return redirect('/paragraphes')->with('success', 'Paragraphe Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paragraphe = Paragraphe::findOrFail($id);
        $paragraphe->delete();

        return redirect('/paragraphes')->with('success', 'Paragraphe Data is successfully deleted');
    }
}
