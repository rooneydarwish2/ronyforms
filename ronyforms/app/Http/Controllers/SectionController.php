<?php
// commande : php artisan make:controller SectionController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Section;
use Illuminate\Support\Facades\Auth;

class SectionController extends Controller
{
    public function create_with_form_id($formulaire_id)
    {
        return view('section/create', ['formulaire_id' => $formulaire_id]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Section::all();
        
        return view('section/index',compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('section/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_form_id(Request $request, $formulaire_id)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
                    'titre' => 'required|max:255',
                    'description' => 'required'
                ]);
        $show = new Section($validatedData);
        $show->user_id = $user->id;
        $show->formulaire_id = $formulaire_id;
        $show->save();
        
        return redirect('/sections')->with('success', 'Section is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
                    'titre' => 'required|max:255',
                    'description' => 'required'
                ]);
        $show = new Section($validatedData);
        $show->user_id = $user->id;
        $show->formulaire_id = 1;
        $show->save();
        
        return redirect('/sections')->with('success', 'Section is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $section = Section::findOrFail($id);
        return view('section/show',compact('section'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section = Section::findOrFail($id);

        return view('section/edit', compact('section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'titre' => 'required|max:255',
            'description' => 'required'
        ]);
        Section::whereId($id)->update($validatedData);

        return redirect('/sections')->with('success', 'Section Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::findOrFail($id);
        $section->delete();

        return redirect('/sections')->with('success', 'Section Data is successfully deleted');
    }
}
