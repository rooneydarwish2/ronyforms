<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|  commande : php artisan route: list
*/

// Laravel Auth, credits to : https://www.itsolutionstuff.com/post/laravel-custom-login-and-registration-exampleexample.html

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post'); 
Route::get('registration', [AuthController::class, 'registration'])->name('register');
Route::post('post-registration', [AuthController::class, 'postRegistration'])->name('register.post'); 
Route::get('dashboard', [AuthController::class, 'dashboard']); 
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::get('/', 'HomeController@home')->name('home');

// Formilaire routes
Route::resource('formulaires', 'FormulaireController');

// Section routes
Route::get('sections/create/{formulaire_id}/', 'SectionController@create_with_form_id')->name('sections.create_with_form_id');
Route::post('sections/store/{formulaire_id}/', 'SectionController@store_with_form_id')->name('sections.store_with_form_id');
Route::resource('sections', 'SectionController');

// SousSection routes
Route::get('sous_sections/create/{section_id}/', 'SousSectionController@create_with_section_id')->name('sous_sections.create_with_section_id');
Route::post('sous_sections/store/{section_id}/', 'SousSectionController@store_with_section_id')->name('sous_sections.store_with_section_id');
Route::resource('sous_sections', 'SousSectionController');

// Widget routes
Route::get('widgets/create/{sous_section_id}/', 'WidgetController@create_with_sous_section_id')->name('widgets.create_with_sous_section_id');
Route::post('widgets/store/{sous_section_id}/', 'WidgetController@store_with_sous_section_id')->name('widgets.store_with_sous_section_id');
Route::resource('widgets', 'WidgetController');

// ReponseCourte routes
Route::get('reponse_courtes/create/{widget_id}/', 'ReponseCourteController@create_with_widget_id')->name('reponse_courtes.create_with_widget_id');
Route::post('reponse_courtes/store/{widget_id}/', 'ReponseCourteController@store_with_widget_id')->name('reponse_courtes.store_with_widget_id');
Route::resource('reponse_courtes', 'ReponseCourteController');

// Paragraphe routes
Route::get('paragraphes/create/{widget_id}/', 'ParagrapheController@create_with_widget_id')->name('paragraphes.create_with_widget_id');
Route::post('paragraphes/store/{widget_id}/', 'ParagrapheController@store_with_widget_id')->name('paragraphes.store_with_widget_id');
Route::resource('paragraphes', 'ParagrapheController');

// ChoixMultiple routes
Route::get('choix_multiples/create/{widget_id}/', 'ChoixMultipleController@create_with_widget_id')->name('choix_multiples.create_with_widget_id');
Route::post('choix_multiples/store/{widget_id}/', 'ChoixMultipleController@store_with_widget_id')->name('choix_multiples.store_with_widget_id');
Route::resource('choix_multiples', 'ChoixMultipleController');

// EchelleLineaire routes
Route::get('echelle_lineaires/create/{widget_id}/', 'EchelleLineaireController@create_with_widget_id')->name('echelle_lineaires.create_with_widget_id');
Route::post('echelle_lineaires/store/{widget_id}/', 'EchelleLineaireController@store_with_widget_id')->name('echelle_lineaires.store_with_widget_id');
Route::resource('echelle_lineaires', 'EchelleLineaireController');

// ListeDiroulante routes
Route::get('liste_diroulantes/create/{widget_id}/', 'ListeDiroulanteController@create_with_widget_id')->name('liste_diroulantes.create_with_widget_id');
Route::post('liste_diroulantes/store/{widget_id}/', 'ListeDiroulanteController@store_with_widget_id')->name('liste_diroulantes.store_with_widget_id');
Route::resource('liste_diroulantes', 'ListeDiroulanteController');

// CasesAcoche routes
Route::get('cases_acochers/create/{widget_id}/', 'CasesAcocherController@create_with_widget_id')->name('cases_acochers.create_with_widget_id');
Route::post('cases_acochers/store/{widget_id}/', 'CasesAcocherController@store_with_widget_id')->name('cases_acochers.store_with_widget_id');
Route::resource('cases_acochers', 'CasesAcocherController');

// DateHeure routes
Route::get('date_heures/create/{widget_id}/', 'DateHeureController@create_with_widget_id')->name('date_heures.create_with_widget_id');
Route::post('date_heures/store/{widget_id}/', 'DateHeureController@store_with_widget_id')->name('date_heures.store_with_widget_id');
Route::resource('date_heures', 'DateHeureController');

// Reponse routes
Route::get('reponses/create/{widget_id}/', 'ReponseController@create_with_widget_id')->name('reponses.create_with_widget_id');
Route::post('reponses/store/{widget_id}/', 'ReponseController@store_with_widget_id')->name('reponses.store_with_widget_id');
Route::resource('reponses', 'ReponseController');