@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details ChoixMultiple') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('choix_multiples.edit', $choix_multiple->id) }}">Edit</a>
                        <form action="{{ route('choix_multiples.destroy', $choix_multiple->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $choix_multiple->contenu }} :</strong> 
                    </li>
                        <p>{{ $choix_multiple->selectionne }} </p>
                        <br> Added at {{  $choix_multiple->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection