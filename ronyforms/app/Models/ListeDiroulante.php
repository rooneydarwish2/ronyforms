<?php
/// php artisan make:model ListeDiroulante -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListeDiroulante extends Model
{
    use HasFactory;

    protected $fillable = ['contenu', 'selectionne'];

    // ManyToOne
    public function widget(){
        return $this->belongsTo(Widget::class);
    }
}
