@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details Widget') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('widgets.edit', $widget->id) }}">Edit</a>
                        <form action="{{ route('widgets.destroy', $widget->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $widget->id }} :</strong> 
                    </li>
                        
                        <br> Added at {{  $widget->created_at->diffForHumans() }}
                        @foreach($widget->choix_multiples as $choix_multiple)
                            <li>{{ $choix_multiple->contenu }}</li>
                        @endforeach
                        <a href="{{ route('choix_multiples.create_with_widget_id', $widget->id) }}">+ Choix</a>
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection