@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details CasesAcoche') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('cases_acochers.edit', $cases_acocher->id) }}">Edit</a>
                        <form action="{{ route('cases_acochers.destroy', $cases_acocher->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $cases_acocher->contenu }} :</strong> 
                    </li>
                        <p>{{ $cases_acocher->coche }} </p>
                        <br> Added at {{  $cases_acocher->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection