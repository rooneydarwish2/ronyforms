<?php
//  php artisan make:controller ListeDiroulanteController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ListeDiroulante;
class ListeDiroulanteController extends Controller
{
    public function create_with_widget_id($widget_id)
    {
        return view('liste_diroulante/create', 
                    ['widget_id' => $widget_id]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $liste_diroulantes = ListeDiroulante::all();
        
        return view('liste_diroulante/index',compact('liste_diroulantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('liste_diroulante/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_widget_id(Request $request, $widget_id)
    {
        $validatedData = $request->validate([
                    'contenu' => 'required|max:100',
                    'selectionne' => 'required'
                ]);
        $show = new ListeDiroulante($validatedData);
        $show->widget_id = $widget_id;
        $show->save();
        
        return redirect('/liste_diroulantes')->with('success', 'ListeDiroulante is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
            'contenu' => 'required|max:100',
            'selectionne' => 'required'
        ]);
        $show = new ListeDiroulante($validatedData);
        $show->user_id = $user->id;
        $show->section_id = 1;
        $show->save();
        
        return redirect('/liste_diroulantes')->with('success', 'ListeDiroulante is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $liste_diroulante = ListeDiroulante::findOrFail($id);
        return view('liste_diroulante/show',compact('liste_diroulante'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $liste_diroulante = ListeDiroulante::findOrFail($id);

        return view('liste_diroulante/edit', compact('liste_diroulante'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'contenu' => 'required|max:100',
            'selectionne' => 'required'
        ]);
        ListeDiroulante::whereId($id)->update($validatedData);

        return redirect('/liste_diroulantes')->with('success', 'ListeDiroulante Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $liste_diroulante = ListeDiroulante::findOrFail($id);
        $liste_diroulante->delete();

        return redirect('/liste_diroulantes')->with('success', 'ListeDiroulante Data is successfully deleted');
    }
}
