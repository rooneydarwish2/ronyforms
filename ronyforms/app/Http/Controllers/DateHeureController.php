<?php
// php artisan make:controller DateHeureController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DateHeure;

class DateHeureController extends Controller
{
    public function create_with_widget_id($widget_id)
    {
        return view('date_heure/create', 
                    ['widget_id' => $widget_id]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date_heures = DateHeure::all();
        
        return view('date_heure/index',compact('date_heures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('date_heure/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_widget_id(Request $request, $widget_id)
    {
        $validatedData = $request->validate([
                    'valeur' => 'required'
                ]);
        $show = new DateHeure($validatedData);
        $show->widget_id = $widget_id;
        $show->save();
        
        return redirect('/date_heures')->with('success', 'DateHeure is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
            'valeur' => 'required'
        ]);
        $show = new DateHeure($validatedData);
        $show->user_id = $user->id;
        $show->section_id = 1;
        $show->save();
        
        return redirect('/date_heures')->with('success', 'DateHeure is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $date_heure = DateHeure::findOrFail($id);
        return view('date_heure/show',compact('date_heure'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $date_heure = DateHeure::findOrFail($id);

        return view('date_heure/edit', compact('date_heure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'valeur' => 'required'
        ]);
        DateHeure::whereId($id)->update($validatedData);

        return redirect('/date_heures')->with('success', 'DateHeure Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $date_heure = DateHeure::findOrFail($id);
        $date_heure->delete();

        return redirect('/date_heures')->with('success', 'DateHeure Data is successfully deleted');
    }
}
