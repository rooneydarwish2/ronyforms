@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details SousSection') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('sous_sections.edit', $sous_section->id) }}">Edit</a>
                        <form action="{{ route('sous_sections.destroy', $sous_section->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $sous_section->titre }} :</strong> 
                    </li>
                        <p>{{ $sous_section->description }} </p>
                        <br> Added at {{  $sous_section->created_at->diffForHumans() }}
                        @foreach($sous_section->widgets as $widget)
                            <li>{{ $sous_widget->titre }}</li>
                        @endforeach
                        <a href="{{ route('widgets.create_with_sous_section_id', $sous_section->id) }}">+ widget</a>
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection