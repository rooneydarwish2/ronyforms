@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Liste des ReponseCourtes') }}</div>
  
                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <ol>
                        @foreach($reponse_courtes as $reponse_courte)
                            <a href="{{ route('reponse_courtes.show', $reponse_courte->id) }}"><li>  <strong>{{ $reponse_courte->contenu }} :</strong></a>
                           
                            <br> Added at {{  $reponse_courte->created_at->diffForHumans() }}
                            <br>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection