@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details ReponseCourte') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('reponse_courtes.edit', $reponse_courte->id) }}">Edit</a>
                        <form action="{{ route('reponse_courtes.destroy', $reponse_courte->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $reponse_courte->contenu }} :</strong> 
                    </li>
                        <br> Added at {{  $reponse_courte->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection