@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Liste des Formulaires') }}</div>
  
                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <ol>
                        @foreach($formulaires as $formulaire)
                            <a href="{{ route('formulaires.show',$formulaire->id) }}"><li>  <strong>{{ $formulaire->titre }} :</strong></a>
                            {{ $formulaire->cadre }} </li>
                            <p>{{ $formulaire->description }}</p>
                            <br> Added at {{  $formulaire->created_at->diffForHumans() }}
                            <br>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection