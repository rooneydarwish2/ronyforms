<?php
// https://github.com/sudeepmanasali/Google-Form-Clone-ReactJS-NodeJS-/tree/main/src
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Messages;

class HomeController extends Controller
{
    public function home() {
        $messages = Messages::all();
        return view('index',
                    ['messages' => $messages]
                );
    }

    
    
}