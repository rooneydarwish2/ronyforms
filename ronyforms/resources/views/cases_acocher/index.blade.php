@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Liste des CasesAcoche') }}</div>
  
                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <ol>
                        @foreach($cases_acochers as $cases_acocher)
                            <a href="{{ route('cases_acochers.show', $cases_acocher->id) }}"><li>  <strong>{{ $cases_acocher->contenu }} :</strong></a>
                           
                            <p>{{ $cases_acocher->coche }}</p>
                            <br> Added at {{  $cases_acocher->created_at->diffForHumans() }}
                            <br>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection