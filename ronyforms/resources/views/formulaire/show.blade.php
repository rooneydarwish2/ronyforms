@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details Formulaire') }}</div>
  
                <div class="card-body">
                    <li>  
                        <strong>{{ $formulaire->titre }} :</strong>  {{ $formulaire->cadre }} 
                    </li>
                        <a href="{{ route('formulaires.edit', $formulaire->id) }}">Edit</a>
                        <form action="{{ route('formulaires.destroy', $formulaire->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        <p>{{ $formulaire->description }} </p>
                        <!--<br> Added at {{  $formulaire->created_at->diffForHumans() }}-->
                        @foreach($formulaire->sections as $section)
                            <fieldset class="sec_fieldset">
                                <legend>{{ $section->titre }}</legend>
                                <p>{{ $section->description }} </p>
                                <!--Added at {{  $section->created_at->diffForHumans() }}-->
                                @foreach($section->sous_sections as $sous_section)
                                    <fieldset class="ssec_fieldset">
                                        <legend>{{ $sous_section->titre }}</legend>
                                        <p>{{ $sous_section->description }} </p>
                                        <!--Added at {{  $sous_section->created_at->diffForHumans() }}-->
                                        @foreach($sous_section->widgets as $widget)
                                            <fieldset class="widget_fieldset">
                                                <!--<legend>{{ $widget->id }}</legend>-->
                                                <!--<p>{{ $widget->user_id }} </p>-->
                                                <!--Added at {{  $widget->created_at->diffForHumans() }}-->
                                                <fieldset>
                                                    @foreach($widget->choix_multiples as $choix_multiple)
                                                        <label>{{ $choix_multiple->contenu }} : </label>
                                                        <input type="radio" name="{{ $choix_multiple->contenu }}" id="{{ $choix_multiple->contenu }}">
                                                    @endforeach
                                                    @foreach($widget->cases_acochers as $cases_acocher)
                                                        <label>{{ $cases_acocher->contenu }} : </label>
                                                        <input type="checkbox" name="{{ $cases_acocher->contenu }}" id="{{ $cases_acocher->contenu }}">
                                                    @endforeach
                                                    @foreach($widget->date_heures as $date_heure)
                                                        <label>{{ $date_heure->valeur }} : </label>
                                                        <input type="datetime-local" name="{{ $date_heure->valeur }}" id="{{ $date_heure->valeur }}">
                                                    @endforeach
                                                    @foreach($widget->liste_diroulantes as $liste_diroulante)
                                                        <label>{{ $liste_diroulante->contenu }} :</label>
                                                        <select id="types_widget" name="type_widget_id">
                                                                <option value="test"> {{ $liste_diroulante->contenu }} </option>
                                                        </select>
                                                    @endforeach
                                                    @foreach($widget->reponse_courtes as $reponse_courte)
                                                        <label>{{ $reponse_courte->contenu }} :</label>
                                                        <input type="text" name="{{ $reponse_courte->contenu }}" id="{{ $reponse_courte->contenu }}">
                                                    @endforeach
                                                    @foreach($widget->paragraphes as $paragraphe)
                                                        <label for="{{ $paragraphe->contenu }}">{{ $paragraphe->contenu }} :</label>
                                                        <textarea id="{{ $paragraphe->contenu }}" name="{{ $paragraphe->contenu }}">
                                                                It was a dark and stormy night...
                                                        </textarea>
                                                    @endforeach
                                                    @foreach($widget->echelle_lineaires as $echelle_lineaire)
                                                        <label>{{ $echelle_lineaire->contenu }} :</label>
                                                        <input type="range" list="tickmarks">
                                                    @endforeach
                                                </fieldset>
                                            </fieldset>
                                        @endforeach
                                        <a href="{{ route('widgets.create_with_sous_section_id', $sous_section->id) }}">+ Widget</a>
                                    </fieldset>
                                @endforeach
                                <a href="{{ route('sous_sections.create_with_section_id', $section->id) }}">+ SousSection</a>
                            </fieldset>
                        @endforeach
                        <a href="{{ route('sections.create_with_form_id', $formulaire->id) }}">+ Section</a>
                        

                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection