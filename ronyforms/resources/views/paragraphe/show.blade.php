@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details Paragraphe') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('paragraphes.edit', $paragraphe->id) }}">Edit</a>
                        <form action="{{ route('paragraphes.destroy', $paragraphe->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $paragraphe->contenu }} :</strong> 
                    </li>
                        <br> Added at {{  $paragraphe->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection