@extends('layout')

@section('title', 'Home')

@section('content')
<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Veuillez renseigner les informations du section</div>
                  <div class="card-body">
  
                      <form action="{{ route('sections.update', $section->id) }}" method="POST">
                          @csrf
                          @method('PUT')
                          
                          <div class="form-group row">
                              <label for="titre" class="col-md-4 col-form-label text-md-right">Titre</label>
                              <div class="col-md-6">
                                  <input type="text" id="titre" class="form-control" value="{{ $section->titre }}" name="titre" required autofocus>
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                              <div class="col-md-6">
                                  <input type="description" id="description" class="form-control" value="{{ $section->description }}" name="description" required>
                              </div>
                          </div>
  
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Submitter
                              </button>
                          </div>
                      </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
@endsection