<?php
// commande : php artisan make:controller ReponseController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reponse;

class ReponseController extends Controller
{
    public function create_with_widget_id($widget_id)
    {
        return view('reponse/create', 
                    ['widget_id' => $widget_id]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reponses = Reponse::all();
        
        return view('reponse/index',compact('reponses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reponse/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_widget_id(Request $request, $widget_id)
    {
        $show = new Reponse();
        $show->widget_id = $widget_id;
        $show->save();
        
        $validatedData = $request->validate([
            'valeur' => 'required'
        ]);
        
        $repondre = new Repondre($validatedData);
        $repondre->user_id = $user->id;
        $repondre->reponse_id = $reponse->id;
        $repondre->save(); 
        
        return redirect('/reponses')->with('success', 'Reponse is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reponse = Reponse::findOrFail($id);
        return view('reponse/show',compact('reponse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reponse = Reponse::findOrFail($id);

        return view('reponse/edit', compact('reponse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reponse = Reponse::findOrFail($id);
        $reponse->delete();

        return redirect('/reponses')->with('success', 'Reponse Data is successfully deleted');
    }
}
