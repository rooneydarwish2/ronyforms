<?php
// commande : php artisan make:controller FormulaireController --resource
// tuto : https://appdividend.com/2020/10/13/laravel-8-crud-tutorial-example-step-by-step-from-scratch/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Formulaire;
use Illuminate\Support\Facades\Auth;

class FormulaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formulaires = Formulaire::all();
        
        return view('formulaire/index',compact('formulaires'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('formulaire/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
                    'titre' => 'required|max:255',
                    'cadre' => 'required',
                    'description' => 'required'
                ]);

        $show = new Formulaire($validatedData);
        $show->save(); 

        $lier = new Lier($validatedData);
        $lier->user_id = $user->id;
        $lier->formulaire_id = $formulaire->id;
        $lier->save(); 
        
        
        return redirect('/formulaires')->with('success', 'Formulaire is successfully saved');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formulaire = Formulaire::findOrFail($id);
        return view('formulaire/show',compact('formulaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $formulaire = Formulaire::findOrFail($id);

        return view('formulaire/edit', compact('formulaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'titre' => 'required|max:255',
            'cadre' => 'required',
            'description' => 'required'
        ]);
        Formulaire::whereId($id)->update($validatedData);

        return redirect('/formulaires')->with('success', 'Formulaire Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $formulaire = Formulaire::findOrFail($id);
        $formulaire->delete();

        return redirect('/formulaires')->with('success', 'Formulaire Data is successfully deleted');
    }
}
