<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SousSection extends Model
{
    use HasFactory;

    protected $fillable = ['titre', 'description'];

    // OneToMany
    public function widgets(){
        return $this->hasMany(Widget::class);
    }

    // ManyToOne
    public function section(){
        return $this->belongsTo(Section::class);
    }
}
