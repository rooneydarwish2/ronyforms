@extends('layout')

@section('title', 'Home')

@section('content')
<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Veuillez renseigner les informations du widget</div>
                  <div class="card-body">

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif

                    <form action="{{ route('widgets.store_with_sous_section_id', $sous_section_id) }}" method="POST">
                          @csrf
                          <div class="form-group row">
                              <label for="type_widget" class="col-md-4 col-form-label text-md-right">Type Widget</label>
                              <div class="col-md-6">
                                <select id="types_widget" name="type_widget_id">
                                    @foreach ($types_widget as $type)
                                        <option value="{{$type->id}}"> {{$type->titre}} </option>
                                    @endforeach
                                </select>
                              </div>
                          </div>
  
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Confirmer
                              </button>
                          </div>
                    </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
@endsection