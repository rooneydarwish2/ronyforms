@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Liste des EchelleLineaires') }}</div>
  
                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <ol>
                        @foreach($echelle_lineaires as $echelle_lineaire)
                            <a href="{{ route('echelle_lineaires.show', $echelle_lineaire->id) }}"><li>  <strong>{{ $echelle_lineaire->debut }} :</strong></a>
                           
                            <p>{{ $echelle_lineaire->fin }}</p>
                            <br> Added at {{  $echelle_lineaire->created_at->diffForHumans() }}
                            <br>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection