<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;
    protected $fillable = ['titre', 'description'];

    // OneToMany
    public function sous_sections(){
        return $this->hasMany(SousSection::class);
    }

    // ManyToOne
    public function formulaire(){
        return $this->belongsTo(Formulaire::class);
    }
}
