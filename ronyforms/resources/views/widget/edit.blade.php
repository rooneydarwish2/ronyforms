@extends('layout')

@section('title', 'Home')

@section('content')
<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Veuillez renseigner les informations du widget</div>
                  <div class="card-body">
  
                      <form action="{{ route('widgets.update', $widget->id) }}" method="POST">
                          @csrf
                          @method('PUT')
                          <select id="types_widget" name="type_widget_id">
                                    @foreach ($types_widget as $type)
                                        <option value="{{$type->id}}"> {{$type->titre}} </option>
                                    @endforeach
                                </select>
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Submitter
                              </button>
                          </div>
                      </form>
                        
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
@endsection