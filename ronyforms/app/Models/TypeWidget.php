<?php
// commande : php artisan make:model TypeWidget -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeWidget extends Model
{
    use HasFactory;

    // OneToMany
    public function widgets(){
        return $this->hasMany(Widget::class);
    }

}
