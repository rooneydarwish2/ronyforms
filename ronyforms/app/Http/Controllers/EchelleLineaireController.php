<?php
// commande :  php artisan make:controller EchelleLineaireController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EchelleLineaire;

class EchelleLineaireController extends Controller
{

    public function create_with_widget_id($widget_id)
    {
        return view('echelle_lineaire/create', 
                    ['widget_id' => $widget_id]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $echelle_lineaires = EchelleLineaire::all();
        
        return view('echelle_lineaire/index',compact('echelle_lineaires'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('echelle_lineaire/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_widget_id(Request $request, $widget_id)
    {
        $validatedData = $request->validate([
            'debut' => 'required',
            'fin' => 'required'
                ]);
        $show = new EchelleLineaire($validatedData);
        $show->widget_id = $widget_id;
        $show->save();
        
        return redirect('/echelle_lineaires')->with('success', 'EchelleLineaire is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
            'debut' => 'required',
            'fin' => 'required'
        ]);
        $show = new EchelleLineaire($validatedData);
        $show->user_id = $user->id;
        $show->section_id = 1;
        $show->save();
        
        return redirect('/echelle_lineaires')->with('success', 'EchelleLineaire is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $echelle_lineaire = EchelleLineaire::findOrFail($id);
        return view('echelle_lineaire/show',compact('echelle_lineaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $echelle_lineaire = EchelleLineaire::findOrFail($id);

        return view('echelle_lineaire/edit', compact('echelle_lineaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'debut' => 'required',
            'fin' => 'required'
        ]);
        EchelleLineaire::whereId($id)->update($validatedData);

        return redirect('/echelle_lineaires')->with('success', 'EchelleLineaire Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $echelle_lineaire = EchelleLineaire::findOrFail($id);
        $echelle_lineaire->delete();

        return redirect('/echelle_lineaires')->with('success', 'EchelleLineaire Data is successfully deleted');
    }
}
