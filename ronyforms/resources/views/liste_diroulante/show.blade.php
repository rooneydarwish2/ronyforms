@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details ListeDiroulante') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('liste_diroulantes.edit', $liste_diroulante->id) }}">Edit</a>
                        <form action="{{ route('liste_diroulantes.destroy', $liste_diroulante->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $liste_diroulante->contenu }} :</strong> 
                    </li>
                        <p>{{ $liste_diroulante->selectionne }} </p>
                        <br> Added at {{  $liste_diroulante->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection