@extends('layout')

@section('title', 'Details Formulaire')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Details Reponse') }}</div>
  
                <div class="card-body">
                    <a href="{{ route('reponses.edit', $reponse->id) }}">Edit</a>
                        <form action="{{ route('reponses.destroy', $reponse->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
            
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    <li>  
                        <strong>{{ $reponse->valeur }} :</strong> 
                    </li>
                        <p>{{ $reponse->selectionne }} </p>
                        <br> Added at {{  $reponse->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    

@endsection