<?php
// php artisan make:model Repondre -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repondre extends Model
{
    use HasFactory;
    protected $fillable = ['valeur'];

    // ManyToOne
    public function user(){
        return $this->belongsTo(User::class);
    }

    // ManyToOne
    public function reponse(){
        return $this->belongsTo(Reponse::class);
    }
}
