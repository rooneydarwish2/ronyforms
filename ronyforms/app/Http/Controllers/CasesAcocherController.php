<?php
// php artisan make:controller CasesAcocherController --resource
namespace App\Http\Controllers;
use App\Models\CasesAcocher;

use Illuminate\Http\Request;

class CasesAcocherController extends Controller
{
    public function create_with_widget_id($widget_id)
    {
        return view('cases_acocher/create', 
                    ['widget_id' => $widget_id]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cases_acochers = CasesAcocher::all();
        
        return view('cases_acocher/index',compact('cases_acochers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cases_acocher/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_widget_id(Request $request, $widget_id)
    {
        $validatedData = $request->validate([
                    'contenu' => 'required|max:100',
                    'coche' => 'required'
                ]);
        $show = new CasesAcocher($validatedData);
        $show->widget_id = $widget_id;
        $show->save();
        
        return redirect('/cases_acochers')->with('success', 'CasesAcocher is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
            'contenu' => 'required|max:100',
            'coche' => 'required'
        ]);
        $show = new CasesAcocher($validatedData);
        $show->user_id = $user->id;
        $show->section_id = 1;
        $show->save();
        
        return redirect('/cases_acochers')->with('success', 'CasesAcocher is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cases_acocher = CasesAcocher::findOrFail($id);
        return view('cases_acocher/show',compact('cases_acocher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cases_acocher = CasesAcocher::findOrFail($id);

        return view('cases_acocher/edit', compact('cases_acocher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'contenu' => 'required|max:100',
            'coche' => 'required'
        ]);
        CasesAcocher::whereId($id)->update($validatedData);

        return redirect('/cases_acochers')->with('success', 'CasesAcocher Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cases_acocher = CasesAcocher::findOrFail($id);
        $cases_acocher->delete();

        return redirect('/cases_acochers')->with('success', 'CasesAcocher Data is successfully deleted');
    }
}
