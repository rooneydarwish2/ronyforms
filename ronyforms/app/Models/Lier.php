<?php
//  php artisan make:model Lier -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lier extends Model
{
    use HasFactory;
    protected $fillable = ['titre', 'description'];

    // ManyToOne
    public function user(){
        return $this->belongsTo(User::class);
    }

    // ManyToOne
    public function formulaire(){
        return $this->belongsTo(Formulaire::class);
    }
}
