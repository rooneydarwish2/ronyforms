<?php
// php artisan make:model CasesAcocher -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasesAcocher extends Model
{
    use HasFactory;

    protected $fillable = ['contenu', 'coche'];

    // ManyToOne
    public function widget(){
        return $this->belongsTo(Widget::class);
    }
}
