<?php
// php artisan make:model DateHeure -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DateHeure extends Model
{
    use HasFactory;

    protected $fillable = ['valeur'];

    // ManyToOne
    public function widget(){
        return $this->belongsTo(Widget::class);
    }
}
