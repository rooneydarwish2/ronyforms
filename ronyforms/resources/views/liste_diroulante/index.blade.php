@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Liste des ListeDiroulantes') }}</div>
  
                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <ol>
                        @foreach($liste_diroulantes as $liste_diroulante)
                            <a href="{{ route('liste_diroulantes.show', $liste_diroulante->id) }}"><li>  <strong>{{ $liste_diroulante->contenu }} :</strong></a>
                           
                            <p>{{ $liste_diroulante->selectionne }}</p>
                            <br> Added at {{  $liste_diroulante->created_at->diffForHumans() }}
                            <br>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection