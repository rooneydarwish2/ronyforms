@extends('layout')

@section('title', 'Home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Liste des ChoixMultiples') }}</div>
  
                <div class="card-body">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                        {{ session()->get('success') }}  
                        </div><br />
                    @endif
                    <ol>
                        @foreach($choix_multiples as $choix_multiple)
                            <a href="{{ route('choix_multiples.show', $choix_multiple->id) }}"><li>  <strong>{{ $choix_multiple->contenu }} :</strong></a>
                           
                            <p>{{ $choix_multiple->selectionne }}</p>
                            <br> Added at {{  $choix_multiple->created_at->diffForHumans() }}
                            <br>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection