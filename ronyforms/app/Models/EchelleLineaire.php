<?php
// php artisan make:model EchelleLineaire -m
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EchelleLineaire extends Model
{
    use HasFactory;

    protected $fillable = ['contenu', 'debut', 'fin'];

    // ManyToOne
    public function widget(){
        return $this->belongsTo(Widget::class);
    }
}
