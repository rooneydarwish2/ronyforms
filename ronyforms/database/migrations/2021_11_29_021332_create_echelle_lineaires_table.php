<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEchelleLineairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('echelle_lineaires', function (Blueprint $table) {
            $table->id();
            $table->string('contenu', 100);
            $table->integer('debut');
            $table->integer('fin');
            $table->timestamps();
            $table->foreignId('widget_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('echelle_lineaires');
    }
}
