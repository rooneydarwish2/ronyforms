<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    use HasFactory;
    
    // ManyToMany
    public function type_widget(){
        return $this->belongsTo(TypeWidget::class);
    }

    // ManyToOne
    public function sous_section(){
        return $this->belongsTo(SousSection::class);
    }

    // OneToMany
    public function choix_multiples(){
        return $this->hasMany(ChoixMultiple::class);
    }
    
    // OneToMany
    public function reponse_courtes(){
        return $this->hasMany(ReponseCourte::class);
    }

    // OneToMany
    public function fichiers(){
        return $this->hasMany(Fichier::class);
    }

    // OneToMany
    public function date_heures(){
        return $this->hasMany(DateHeure::class);
    }

    
    // OneToMany
    public function paragraphes(){
        return $this->hasMany(Paragraphe::class);
    }

    // OneToMany
    public function cases_acochers(){
        return $this->hasMany(CasesAcocher::class);
    }
    
    // OneToMany
    public function echelle_lineaires(){
        return $this->hasMany(EchelleLineaire::class);
    }

    // OneToMany
    public function liste_diroulantes(){
        return $this->hasMany(ListeDiroulante::class);
    }
}
