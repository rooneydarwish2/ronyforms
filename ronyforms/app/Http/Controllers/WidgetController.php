<?php
// php artisan make:controller WidgetController --resource
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Widget;
use App\Models\TypeWidget;
use Illuminate\Support\Facades\Auth;

class WidgetController extends Controller
{
    public function create_with_sous_section_id($sous_section_id)
    {
        $types_widget = TypeWidget::all();
        return view('widget/create', 
                    ['sous_section_id' => $sous_section_id,
                    'types_widget' => $types_widget
                    ]
                );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $widgets = Widget::all();
        
        return view('widget/index',compact('widgets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('widget/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_with_sous_section_id(Request $request, $sous_section_id)
    {
        $user = Auth::user();

        $show = new Widget();
        $show->user_id = $user->id;
        $show->sous_section_id = $sous_section_id;
        $show->type_widget_id = $request->type_widget_id;
        $show->save();
        
        $type_widget = TypeWidget::findOrFail($request->type_widget_id)->titre;
        
        $route = '';
        if ($type_widget == 'ChoixMultiple'){
            $route = 'choix_multiples.create_with_widget_id';
        } else if ($type_widget == 'ReponseCourte'){
            $route = 'reponse_courtes.create_with_widget_id';
        } else if ($type_widget == 'Paragraphe'){
            $route = 'paragraphes.create_with_widget_id';
        } else if ($type_widget == 'CasesAcoche'){
            $route = 'cases_acochers.create_with_widget_id';
        } else if ($type_widget == 'DateHeure'){
            $route = 'date_heures.create_with_widget_id';
        } else if ($type_widget == 'EchelleLineaire'){
            $route = 'echelle_lineaires.create_with_widget_id';
        } else if ($type_widget == 'ListeDiroulante'){
            $route = 'liste_diroulantes.create_with_widget_id';
        }

        return redirect()->route($route, $show->id)->with('success', 'Widget is successfully saved');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $show = new Widget();
        $show->user_id = $user->id;
        $show->sous_section_id = 1;
        $show->save();
        
        return redirect('/widgets')->with('success', 'Widget is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $widget = Widget::findOrFail($id);
        return view('widget/show',compact('widget'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $widget = Widget::findOrFail($id);
        $types_widget = TypeWidget::all();

        return view('widget/edit', compact('widget'),
                    [
                        'types_widget' => $types_widget
                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        Widget::whereId($id)->update(['type_widget_id'=> $request->type_widget_id]);

        return redirect('/widgets')->with('success', 'Widget Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $widget = Widget::findOrFail($id);
        $widget->delete();

        return redirect('/widgets')->with('success', 'Widget Data is successfully deleted');
    }
}
